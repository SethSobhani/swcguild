function luckySevens() {

    var startingBet = document.getElementById("startingBet").value;
    startingBet = parseInt(startingBet);


    if (startingBet > 0) {

        var dice1 = 0;
        var dice2 = 0;
        var diceTotal = 0;
        var losecount = 0;
        var win = 4;
        var loss = 1;
        var maxMoneyWon = 0;
        var allMoneyNumbers = [startingBet];
        var totalMoney = startingBet;

        do {
            // random dice
            dice1 = Math.floor(Math.random() * 6) + 1;
            dice2 = Math.floor(Math.random() * 6) + 1;
            diceTotal = dice1 + dice2;

            // win
            if (diceTotal == 7) {
                totalMoney = totalMoney + win;
                var allMoneyNumbers_last = allMoneyNumbers[allMoneyNumbers.length - 1];
                var allMoneyNumbers_new = allMoneyNumbers_last + win;
                allMoneyNumbers.push(allMoneyNumbers_new);

                // lose
            } else {
                totalMoney--;
                losecount++;
                var allMoneyNumbers_last = allMoneyNumbers[allMoneyNumbers.length - 1];
                var allMoneyNumbers_new = allMoneyNumbers_last - loss;
                allMoneyNumbers.push(allMoneyNumbers_new);
            }
        }
        while (totalMoney > 0);


        var bigestWin;
        bigestWin = Math.max.apply(null, allMoneyNumbers);
        bigestWinIndex = allMoneyNumbers.indexOf(bigestWin);

        var result = document.getElementById("result");
        result.style.visibility = "visible";
        var rollPlayed = allMoneyNumbers.length - 1;
        var topHit = bigestWinIndex + 1;

        document.getElementById("result").innerHTML = ("<center><table><th  colspan=\"2\"; class=\"th1\">Results</th><tr><th class=\"th2\">Starting Bet</th><th class=\"th2\">$" + startingBet + ".00</th></tr><tr><td>Total Rolls Before Going Broke</td><td>" + rollPlayed + "</td></tr><tr><td>Highes Amount Won</td><td>" + bigestWin + "</td></tr><tr><td>Roll Count at Highest Amount Won</td><td>" + topHit + "</td></tr></table></center>");
        document.getElementById("button").innerHTML = "Feeling Lucky !!";

    } else {
        alert("Was that a number???!!!");
    }

}
